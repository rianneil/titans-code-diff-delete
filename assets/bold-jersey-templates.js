var BOLD = BOLD || {};

/*==================================================
# Removes tabs from template
    * Removes tabs because options doesn't like them
==================================================*/
BOLD.cleanupTemplate = function(template){
    return template.replace(/\s\s/g,'');
}

/*==================================================
# Reset Templates
==================================================*/
BOLD.options = BOLD.options || {};
BOLD.options.templates = BOLD.options.templates || {};
BOLD.options.templates.single = BOLD.cleanupTemplate('\
    <div class="bold_option bold_option_{{ optionType }} {{#optionValueNoStock}}bold_option_out_of_stock{{/optionValueNoStock}}">\
        {{#optionTitle}}\
            <label>{{{ optionTitle }}}</label>\
        {{/optionTitle}}\
        <div class="selector-wrapper form-group">\
            {{{ optionElement }}}\
        </div>\
    </div>');